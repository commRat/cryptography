# Sword Manager
Sword Manager is password manager, which allows you to generate and store your passwords. It uses salt hashing and cryptohraphy tools to protect your security. First you have to register yourself (name and password) - you will need to remember that password. After the successfull registration you will be able to read stored password (site, nickname, password).

<img src="./data/sword-manager.png">

# Development
Everyone is able to contribute, change and modify the program. Source code files are located in Scripts folder. 
Main console.py is console version (fully working, without a GUI)
Sword Master.py is complete GUI version

# Requirements
- Python 3+
- tkinter 8.6
- bcrypt 3.2.0
- SQLite 3.32.3
- cryptohraphy 2.9.2

# How to run program
1) Dowload all files to one folder 
2) Extract all files in this folder
3) Go to dist folder
4) Run swordmaster.exe

If you experience some trouble with executing a file. Try tu run swordmaster.exe through command line (./swordmaster.exe).
Program should run on most of Linux distributions.

# Video
https://youtu.be/s-3sXxB7vzY

# License
Public domain 
