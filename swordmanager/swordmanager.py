import sqlite3
import random
import bcrypt
from cryptography.fernet import Fernet
import string
import time
import sys
import os
import pkg_resources


db_master =pkg_resources.resource_string(__name__, "data/master.db")
db_sword =pkg_resources.resource_string(__name__, "data/sword.db")
conn_master = sqlite3.connect(db_master)
conn = sqlite3.connect(db_sword)
cm = conn_master.cursor()
c = conn.cursor()
cm.execute('CREATE TABLE IF NOT EXISTS master(user TEXT UNIQUE, password VARCHAR NOT NULL)')
c.execute('CREATE TABLE IF NOT EXISTS sword(master TEXT, site TEXT, user TEXT, password VARCHAR)')


def cls():
    os.system('cls' if os.name=='nt' else 'clear')


def salt_hash(password):
    hashed_pw = bcrypt.hashpw(password, bcrypt.gensalt())
    return hashed_pw


def put_to_db(username, hashed_pw):
    try:
        put = '''INSERT INTO master(user, password)VALUES (?, ?)'''
        cm.execute(put,[(username), (hashed_pw)])
        conn_master.commit()
    except Exception:
        print('Username already exists. Try it again.')
        create_account()


def pull_from_db(username, password):
    cm.execute('SELECT password FROM master WHERE user = ?', (username,))
    hashed_db = cm.fetchone()
    if not hashed_db:
        print('Invalid username.')
        return 0
    key = pkg_resources.resource_string(__name__, "data/sword.key")
    encoded_password = hashed_db[0].encode('utf-8')
    password_input = password.encode('utf-8')
    if bcrypt.checkpw(password_input, encoded_password):
        print('You logged in.')
        logged_in(username, key)
    else:
        print('Incorrect password, try it again.')
        return 0


def login():
    existing_user = input('Enter your username: ')
    existing_password = input('Enter your password: ')
    pull_from_db(existing_user, existing_password)


def create_account():
    while True:
        username = input('Create your username: ')
        inp_pw = input('Create your password: ')
        inp_pw_again = input('Enter your password again: ')
        if len(username) < 1:
            print('Your username can not be empty.')
        elif len(inp_pw) < 1:
            print('Your password can not be empty.')
        elif inp_pw == inp_pw_again:
            password = inp_pw.encode('utf-8')
            hashed = salt_hash(password)
            break
        else:
            print('password does not match, try it again')
    hashed_pw = hashed.decode('utf-8')
    put_to_db(username, hashed_pw)
    print('Account created')


def generate_sword():
    chars = list(string.ascii_letters)
    chars.extend(['!', '#', '$', '%', '&', ')', '(', '*', '+', '-', '@', '<', '>', '^'])
    new_password = []
    generate = input('GENERATE PASSWORD\nPress 1 for fixed length\nPress 2 for automatic length')
    if generate == '1':
        how_much = int(input('Press a number for fixed lenght password: '))
        for i in range(how_much):
            new_password.append(random.choice(chars))
        new_password = ''.join([str(ele) for ele in new_password])
        print(f'New password: {new_password}')
    elif generate == '2':
        password_lenght = random.randint(10,18)
        for i in range(password_lenght):
            new_password.append(random.choice(chars))
        new_password = ''.join([str(ele) for ele in new_password])
        print(f'New password: {new_password}')
    else:
        print('You must put a number.')


def put_sword(master, site, username, hashed_pw):
    put = '''INSERT INTO sword(master, site, user, password)VALUES (?, ?, ?, ?)'''
    c.execute(put,[(master), (site), (username), (hashed_pw)])
    conn.commit()


def pull_sword(master, key):
    c.execute('SELECT password FROM sword WHERE master = ?', (master,))
    sword = c.fetchall()
    sw_items = (list(list(zip(*sword))[0]))
    c.execute('SELECT site FROM sword WHERE master = ?', (master,))
    site = c.fetchall()
    site_items = (list(list(zip(*site))[0]))
    c.execute('SELECT user FROM sword WHERE master = ?', (master,))
    user = c.fetchall()
    user_items = (list(list(zip(*user))[0]))
    f = Fernet(key)
    for i in range(len(user_items)):
        print(f'Site: {site_items[i]}')
        print(f'User: {user_items[i]}')
        decrypted = f.decrypt(sw_items[i])
        decoded = decrypted.decode()
        print(f'Password: {decoded}\n')


def logged_in(master, key):
    while True:
        x = input('1 FOR PRINT, 2 FOR NEW DETAILS, 3 FOR GENERATE PASSWORD, 4 FOR EXIT')
        if x == '1':
            pull_sword(master, key)
        elif x == '2':
            site = input('SITE: ')
            user = input('USERNAME : ')
            pw = input('PASSWORD : ')
            encoded = pw.encode()
            f = Fernet(key)
            password = f.encrypt(encoded)
            put_sword(master, site, user, password)
        elif x == '3':
            generate_sword()
        elif x == '4':
            close()
            break
        else:
            print('INVALID COMMAND')


def close():
    cls()
    print('Thank you for using SWORD MANAGER.')
    print('\n\n\nCreated by: commRat')
    cls()


def main():
    print('WELCOME TO SWORD MANAGER. ')
    while True:
        choice = input('Press 1 if you want to login\nPress 2 if you want to create account\nPress 3 for exit')
        if choice == '1':
            login()
        elif choice == '2':
            create_account()
            login()
        elif choice == '3':
            close()
            break
        else:
            print('Invalid command')


if __name__ == '__main__':
    sys.exit(main())

