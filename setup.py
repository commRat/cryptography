#!/usr/bin/python

from setuptools import setup, find_packages
import pathlib
from setuptools.command.install_scripts import install_scripts
import subprocess
from distutils.util import convert_path
import os
import subprocess

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')
path_ = subprocess.run("pwd", capture_output=True, shell=True).stdout.decode().strip() + "/"
with open(os.path.join(path_, 'swordmanager/VERSION')) as version_file:
    __version__ = version_file.read().strip()


setup(
    name='swordmanager',
    version=__version__,
    description='Password manager. Generating & storing passwords for various sites.',
    long_description=long_description,
    url='https://gitlab.com/commRat/cryptography',
    author='David Tomicek',
    author_email='tomicek.david@yahoo.com',
    keywords='password manager, storing passwords, bcrypt, cli',
    packages=find_packages(),
    package_data={'swordmanager': [
        "VERSION",
        "data/sword.key",
        "data/master.db",
        "data/sword.db",
        ]},
    install_requires=[
        'cryptography',
        'bcrypt',
        ],
    entry_points={"console_scripts": ["swordmanager = swordmanager.swordmanager:main"]},
)
