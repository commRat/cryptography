FROM fedora
RUN dnf install -y ncurses python3-pip python3-bcrypt python3-cryptography
RUN pip install --upgrade setuptools
RUN mkdir home/swordmanager
WORKDIR home/swordmanager
COPY . .
ENTRYPOINT ["python3", "swordmanager/swordmanager.py"]
